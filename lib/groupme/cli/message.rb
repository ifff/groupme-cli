require 'colorize'

module Groupme
  module Cli
    class Message
      attr_reader :sender, :time, :text

      def initialize(sender:, time:, text:)
        @sender, @time, @text = sender, time, text
      end

      def to_s
        "<#{@sender.colorize :blue}[#{fmt_time.colorize :red}]>:\n\t#{fmt_text}"
      end

      private
      def fmt_time
        @time.strftime "%I:%M%p"
      end

      def fmt_text
        @text.lines.join "\t"
      end
    end
  end
end
