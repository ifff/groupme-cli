require 'colorize'

require "groupme/cli/version"
require "groupme/cli/session"

module Groupme
  module Cli
    def self.run token
      puts "Welcome to the GroupMe CLI v#{VERSION}"
      puts "Start by entering '%open chat_name', where chat_name is the name of the chat you want to open."

      sesh = Session.new token

      begin
        # For status messages coming from the input thread
        status_buffer = Array.new

        # input loop/thread
        input_thread = Thread.new do
          loop do
            input = gets
            if input[0..6] == "%image "
              sesh.send_msg("", input[7..-1].strip)
            elsif input.strip == "%groups"
              status_buffer << "Your groups:\n"

              sesh.get_groups.each do |group|
                status_buffer << group["name"]
              end
            elsif input[0..4] == "%quit"
              exit
            elsif input[0..5] == "%open "
              name = input[6..-1].chomp

              if sesh.open_chat(name)
                status_buffer << "<Opened #{name}>"
              else
                status_buffer << "That group does not exist. Check the name you entered to make sure it's correct."
              end
            else
              sesh.send_msg input
            end
          end
        end

        # output loop
        loop do
          # check for signals and catch them in order to exit gracefully
          Signal.trap("INT") { exit }
          Signal.trap("TERM") { exit }

          status_buffer.each { |m| puts m }
          status_buffer.clear

          if sesh.chat_is_open?
            # fetch new messages
            sesh.update_messages
            msgs = sesh.new_messages
            msgs.each {|m| puts m}
          end
        end

      rescue SystemExit
        puts "Goodbye"
      rescue Exception => e
        puts "Something went horribly wrong! See ya later."
        puts "Exception details: #{e}"
      end

    end
  end
end
