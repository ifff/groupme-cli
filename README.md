# Groupme::Cli

This is a command-line interface for the [GroupMe](https://groupme.com) app. It lets you open chats in your terminal, post to them, send images, and switch which chat you're on.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'groupme-cli'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install groupme-cli

## Usage
There's a little script in this app's bin/ directory called bin/groupme that wraps the gem's runner method. Simply execute

		$ bin/groupme MY_ACCESS_TOKEN

Where MY_ACCESS_TOKEN is obtained from the [GroupMe developer site](https://dev.groupme.com) (You'll have to log in to get your token).
When you run the app, it will ask you to open a chat. Just do `%open chat_name` to open a chat. You can repeat that command to switch to a different chat.
To send normal messages, just type your message and hit enter, and it should send and show up. To send images, do `%image /path/to/the/image`.
You can exit by either typing `%quit`, Ctrl+C, or dousing your computer with orange juice until it turns off (the program is equipped to handle sudden termination, but I still don't advise the last method).

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/Bleu-Box/groupme-cli.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Note

I am not affiliated with GroupMe. Additionally, this gem is not an official way of accessing GroupMe, so GroupMe is not liable for any problems you experience specifically with this gem.