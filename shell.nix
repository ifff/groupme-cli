with (import <nixpkgs> {});
let
  env = bundlerEnv {
    name = "groupme-cli-bundler-env";
    inherit ruby;
    gemfile  = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset   = ./gemset.nix;
  };
in stdenv.mkDerivation {
  name = "groupme-cli";
  buildInputs = [ env ];
}
