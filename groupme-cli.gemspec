lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "groupme/cli/version"

Gem::Specification.new do |spec|
  spec.name          = "groupme-cli"
  spec.version       = Groupme::Cli::VERSION
  spec.authors       = ["Benjamin Duchild"]
  spec.email         = ["xxarcajethxx@gmail.com"]

  spec.summary       = %q{A command-line interface for GroupMe}
  spec.homepage      = "https://gitlab.com/Bleu-Box/groupme-cli"
  spec.license       = "MIT"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "colorize", "~> 0.8.1"
  spec.add_development_dependency "groupme", "~> 0.0.7"
  spec.add_development_dependency "hashie", "~> 3.6.0"
end
